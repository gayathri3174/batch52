console.log("hello");
//Data types
//var,const,let
//Math -min,max,pow,abs,random
//function -reusable code
//for loop
//string methods ,array methods

//declaration
function greet(name1,batch){

return "Good Morning"+name1+" "+batch;
}

//function calling
let Name = "Neeraj";
let batchnum = 56
let greeting =greet(Name,batchnum);
greet();
console.log(greeting);


//write a function to perform addition of 3 numberes
function additionOFThreeNum(num1,num2,num3){
    return num1+num2+num3;
}

let result1 = additionOFThreeNum(5,9,12);
let result2 = additionOFThreeNum(100,67,90);
console.log(result1,result2);

//write a function to check whether the given number is even or odd



function evenorodd(num){
    if(num %2 ==0){
        return "Even Number"
       
    }

    return "Odd Number"

}

console.log(evenorodd(7));

//write a function to print the sum of n values

//5  1+2+3+4+5=15
//3  1*2*3


function sumofnumber(n){
    let sum =1;
    for(let i =1 ;i<=n;i++){
        
        sum*=i
    }

    return sum;
}

console.log(sumofnumber(5));


// if else if
// if(condition1){
//   statement1
// }else if(condition2){
//     statement2
// }else{
//     statement 3
// }
//write a function to check whether the given number is negative  

function checknumber(num){
    if(num<0){
        return "Negative value";
    }
    else if(num >0 & num<50){
        return "Between  1 and 50";

    }else {
        return "Above 50";
    }
}


// let number = parseInt(prompt("Enter the value"));
// console.log(typeof(number));  //string
// console.log(checknumber(number));

function Table(number){
    for(let i=1;i<=10;i++){
        console.log(number+"*"+i+ "="+number*i);
    }
}
Table(3);

const numbers = [12,3,6,78,8]

for(let i = 0;i<=numbers.length;i++){
    console.log(numbers[i]);
}










//find the maximum value in the given array
//find the minimum value in the given array
//find the sum of the values in the given array
//write a function to find the reverse of an number

function findmax(arr){
  
  return Math.max(...arr);

}

let array= [1,3,4,50,90,100]
let maxvalue = findmax(array)
console.log(`The maximum value is ${maxvalue}`);


//write a function to write the reverse of a number
//Input:1234
//Output:4321




//for --- where we know how many times we need to execute the statement 
//while -- where we dont know ,when the condition will statisfy,
//until the condition is true it executes the statement .




//reverseofnum 

function reverseOfNum(num){

    let reversenum = 0;
        
    while(num!=0){      
        reversenum =  (reversenum*10)+(num%10);
        num = Math.floor(num/10); 
   }
   return reversenum;

}

let  reversed_num = reverseOfNum(12356784);
console.log(reversed_num)




//write a program to find whether the given number is palindrome or not
//input  131 
//output true

function isPalindrome(usergivennum){
    let reversednum = reverseOfNum(usergivennum);
    return reversednum == usergivennum;


}
console.log(isPalindrome(1311));


//how to reverse a  number,palindrome,sum of numbers,arrays

//write a program in javascript to check whether the given string is palindrome or not

//madam ---madam
//sos --sos
let word = "hello";
console.log(word.split('').reverse().join(''));

function isPalindromeString(word){
      let reversed_word = word.split('').reverse().join('');
    //   if (word == reversed_word){
    //        return true;
    //   }else{
    //     return false
    //   }


      return word == reversed_word
}

console.log(isPalindromeString('jheh'))


function countofnum(num){
    let count =0;
    while(num!=0){ 
        count= count+1;
        num = Math.floor(num/10); 
        
        
    }
    return count ;
}


console.log(`the count of number is ${countofnum(123456789)}`);

 



let num1 =1234;
console.log(Math.floor(num1/10)); 123
console.log(num1%10); 


//sum of numbers  1234 --- 1+2+3+4   -- 4+3+2+1
//product of numbers  1234 --- 1*2*3*4 ----


function sumofdigits(numb){
    let sum = 0;
    while(numb != 0){
        sum = sum +(numb%10);
        numb = Math.floor(numb/10);


    }

    return sum;
}

let sumofdigit = sumofdigits(23454654);
console.log(`Sum of the given number is ${sumofdigit}`);



function productOfDigits(numb){
    let product = 1;
    while(numb != 0){
        product = product *(numb%10);
        numb = Math.floor(numb/10);


    }

    return product;
}

let productofdigit = productOfDigits(2567);
console.log(`Product of the given number is ${productofdigit}`);




//Check whether the given number is prime or not

function isPrime(num){
    if(num<=1){
        return false;
    }
    for (let i =2 ;i<Math.sqrt(num);i++){
        if(num%i ==0){
            return false;
        }
    }

    return true;


}

console.log(isPrime(139));

// print the factors of the number
// sum of factors of the number
// find the perfect number
//print perfect numbers between 100 to 999

// 10/1,10/2,10/3,10/4,
// 1,2,3,4,5,6,7,8,9,10
function factorsOfNumber(num){
   let factors = "";
   for(let i =1;i<num;i++){
       if(num%i ==0){
           factors+=i+" ";
           }

   }


    return factors;
}
console.log(factorsOfNumber(100))








